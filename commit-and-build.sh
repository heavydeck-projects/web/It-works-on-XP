git commit -m "Lazy commit done on: `date`"
make clean && make build && make deploy
find _site -type f | egrep -iv '(\.png$)|(\.jpg$)|(\.gz$)' | xargs -n1 gzip -k -9 -f
#cd _site/ && tar cavvf ../all-site.tar . && mv ../all-site.tar .
