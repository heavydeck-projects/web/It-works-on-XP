# Flash Player

Stand alone player for viewing shockwave flash files on Windows.

## Info & Download

- [Flash player stand-alone homepage](https://www.adobe.com/support/flashplayer/debug_downloads.html)
- By: Adobe (formerly Macromedia)
- Version: 32.0 r0
- [Download link](https://fpdownload.macromedia.com/pub/flashplayer/updaters/32/flashplayer_32_sa.exe)

## Notes

This ***IS NOT*** the flash plugin for the browser, this is the stand-alone
player for windows. For the Flash plugin see the ["Flash Plugin"][flash_plugin]
entry.

## Screenshots

![Flash player](./pics/flash-player/flash-player-00.png)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: flash multimedia

[flash_plugin]: ./flash-plugin.html
