# Flash Plugin

Flash plugin for the browser.

## Info & Download

- By: Adobe (formerly Macromedia)
- Version: 32.0.0.303 (probably)
- *No official download exists as of 2021!*
- Mirror sites. ***Use at your own risk!***
    - `https://i430vx.net/files/misc/`
    - [🧲 Magnet link](magnet:?xt=urn:btih:BAE8680788DA58B0A2BFFA0AA7F5B04791878DF6&dn=Adobe%20Flash%20Player%2032.0.0.207%20%7BB4tman%7D&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.dler.org%3A6969%2Fannounce&tr=udp%3A%2F%2Fopentracker.i2p.rocks%3A6969%2Fannounce&tr=udp%3A%2F%2F47.ip-51-68-199.eu%3A6969%2Fannounce) (V32.0.0.7)
    - [🧲 Magnet link](magnet:?xt=urn:btih:2E8B022570572C75FC73368721521EDE5B9714EC&dn=Adobe%20Flash%20Player%2027.0.0.187%20Final%20-%20ArmaanPC&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969%2Fannounce&tr=udp%3A%2F%2Ftracker.dler.org%3A6969%2Fannounce&tr=udp%3A%2F%2Fopentracker.i2p.rocks%3A6969%2Fannounce&tr=udp%3A%2F%2F47.ip-51-68-199.eu%3A6969%2Fannounce) (V27.0.0.187)

<!-- - [Download link (Stand-Alone)](./mirror/flash-plugin/install_flash_player.exe) -->
<!-- - [Download link (ppapi; Firefox & Chrome)](./mirror/flash-plugin/install_flash_player_ppapi.exe) -->
<!-- - [Download link (ActiveX; Internet Explorer)](./mirror/flash-plugin/install_flash_player_ax.exe) -->

## Notes

Adobe is actively trying to kill flash and prevent users from installing it
on their systems at all costs. [Stand-alone flash player](./flash-player.html)
still seems to have an official download as of 2021.

On some systems it will refuse to install since it is not the latest version,
however, the latest version will not play flash files anyway, so you *MUST*
remove all traces of a previous flash install, disable automatic updates, and
maybe install it *WITHOUT* an internet connection.

![Disable updates](./pics/flash-plugin/flash-plugin-00.png)

![Out of date "error"](./pics/flash-plugin/flash-plugin-01.png)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: browser flash multimedia
