# VcXsrv

VcXsrv is an X server for Windows.

## Info & Download

- [VcXsrv homepage](https://sourceforge.net/projects/vcxsrv/)
- Version: 1.14.2.1 (July 2013)
- [Download link](https://sourceforge.net/projects/vcxsrv/files/vcxsrv/1.14.2.1/)

## Description

(From VcXsrv wiki)   
VcXsrv is an X Server for WinNT. In order to be able to start the graphical
(GUI) version of programs (e.g. xstata = X window version of STATA) on Unix
servers (e.g. of the bwHPC network) under Windows, you need an X server that
is on your local Computer must be installed (e.g. for Putty, Kitty, Smartty
or other ssh-forwarting or in Windows 10 as X-Server Host for Linux
Subsystem's).

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: linux unix X
