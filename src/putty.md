# PuTTY

PuTTY is a terminal emulator and SSH client for Windows.

## Info & Download

- [PuTTY homepage](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- By: [Simon Tatham](https://www.chiark.greenend.org.uk/~sgtatham/)
- Version: 0.74 (Latest version as of post date)
- [Download link](https://the.earth.li/~sgtatham/putty/latest/w32/putty-0.74-installer.msi)

## Description

(From [wikipedia](https://en.wikipedia.org/wiki/PuTTY))   
PuTTY is a free and open-source terminal emulator, serial console and network
file transfer application. It supports several network protocols, including
SCP, SSH, Telnet, rlogin, and raw socket connection. It can also connect to a
serial port.

## Screenshots

![Putty interface](./pics/putty/putty-00.png)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: telnet ssh terminal serial
