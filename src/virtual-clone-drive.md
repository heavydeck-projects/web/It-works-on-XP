# Virtual Clone Drive

Virtual Clone drive allows you to mount optical disk images as if they were a
real CD-ROM drive.

## Info & Download

- [Virtual Clone Drive homepage](https://www.elby.ch/en/products/vcd.html)
- By: [Elaborate Bytes](https://www.elby.ch/)
- Version: 5.5.2.0 (Latest version as of post date)
- [Download link](https://www.elby.ch/download/SetupVCD.exe)

## Description

(From Virtual Clone Drive homepage)   
Virtual CloneDrive works and behaves just like a physical CD, DVD, or Blu-ray
drive, although it only exists virtually. Image files can be "inserted" into the
virtual drive from your harddisk or from a network drive by just a double-click,
and thus be used like a normal CD, DVD, or Blu-ray. Virtual CloneDrive supports
up to 15 virtual drives at the same time - no matter if the image file contains
audio, video or just simple data. Virtual CloneDrive is fully integrated in
Windows Explorer context menus and on top of all it is free!

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: virtual-drive
