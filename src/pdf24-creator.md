# PDF24 Creator

PDF24 Creator is a PDF virtual printer for Windows.

## Info & Download

- [PDF24 homepage](https://www.pdf24.org)
- By: Geek Software GmbH
- Version: 7.9.0
- [Download link](https://creator.pdf24.org/download/?version=7.9.0&tou=msi)

## Description

(From PDF24 Creator homepage)   
Free and easy to use PDF creator with many features.

- Merge or split PDF files
- Add, remove, extract, rotate, sort and move PDF pages
- Import and automatic conversion of documents (Word, Excel, images, etc. to PDF
- Various preview modes for easy editing of PDF documents
- Integrated viewer
- Drag & Drop wherever possible
- Follow up tools: Save, Print, Email, Fax, ...

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: pdf printer
