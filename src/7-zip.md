# 7-Zip

7-Zip is a file archiver with a high compression ratio.

## Info & Download

- [7-Zip homepage](https://www.7-zip.org)
- By: Igor Pavlov
- Version: 19.00 (Latest version as of post date)
- [Download link](https://www.7-zip.org/a/7z1900.exe)

## Description

(From 7-zip homepage)   
The main features of 7-Zip

- High compression ratio in 7z format with LZMA and LZMA2 compression
- Packing / unpacking: 7z, XZ, BZIP2, GZIP, TAR, ZIP and WIM
- Unpacking only: AR, ARJ, CAB, CHM, CPIO, CramFS, DMG, EXT, FAT, GPT, HFS, IHEX, ISO, LZH, LZMA, MBR, MSI, NSIS, NTFS, QCOW2, RAR, RPM, SquashFS, UDF, UEFI, VDI, VHD, VMDK, WIM, XAR and Z.
- For ZIP and GZIP formats, 7-Zip provides a compression ratio that is 2-10 % better than the ratio provided by PKZip and WinZip
- Strong AES-256 encryption in 7z and ZIP formats
- Self-extracting capability for 7z format
- Integration with Windows Shell
- Powerful File Manager
- Powerful command line version
- Plugin for FAR Manager
- Localizations for 87 languages


## Screenshots

![7-zip UI](./pics/7-zip/7-zip-00.png)

## Contribution
- J.Luis ([homepage](https://heavydeck.net/))

;tags: archiving compression
