# Veracrypt

An open-source and multiplatform full-disk encryption solution with many
features. Based on the code of the former TrueCrypt software.

## Info & Download

- [Veracrypt homepage](https://veracrypt.fr/)
- Version: 1.22 (March 2013)
- [Download link](https://sourceforge.net/projects/veracrypt/files/VeraCrypt%201.22/VeraCrypt%20Setup%201.22.exe/download)

## Description

(From Veracrypt homepage)   
VeraCrypt main features:

- Creates a virtual encrypted disk within a file and mounts it as a real disk.
- Encrypts an entire partition or storage device such as USB flash drive or hard drive.
- Encrypts a partition or drive where Windows is installed (pre-boot authentication).
- Encryption is automatic, real-time(on-the-fly) and transparent.
- Parallelization and pipelining allow data to be read and written as fast as if the drive was not encrypted.
- Encryption can be hardware-accelerated on modern processors.
- Provides plausible deniability, in case an adversary forces you to reveal the password: Hidden volume (steganography) and hidden operating system.

## Screenshots

![Veracrypt main interface](./pics/veracrypt/veracrypt-00.png)

Veracrypt main interface.

![Veracrypt volume creation dialog](./pics/veracrypt/veracrypt-01.png)

Volume creation dialog.

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: disk-encryption security
