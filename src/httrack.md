# HTTrack website copier

HTTrack allows you to download an entire site to your local machine and
use it offline.

## Info & Download

- [HHTrack homepage](https://www.httrack.com)
- By: Xavier Roche & other contributors
- Version: 3.49-2 (Latest version as of post date)
- [Download link](https://www.httrack.com/page/2/en/index.html)

## Description

(From HTTrack Drive homepage)   
HTTrack is a free (GPL, libre/free software) and easy-to-use offline browser
utility.

It allows you to download a World Wide Web site from the Internet to a local
directory, building recursively all directories, getting HTML, images, and
other files from the server to your computer. HTTrack arranges the original
site's relative link-structure. Simply open a page of the "mirrored" website
in your browser, and you can browse the site from link to link, as if you
were viewing it online. HTTrack can also update an existing mirrored site,
and resume interrupted downloads. HTTrack is fully configurable, and has an
integrated help system.

## Screenshots

![HTTrack project menu](./pics/httrack/httrack-00.png)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: internet
