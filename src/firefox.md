# Mozilla Firefox

A free and open source Web Browser.

## Info & Download

- [Firefox homepage](https://www.mozilla.org/)
- By: Mozilla
- Version: 52.9.0 ESR
- [Download link](https://ftp.mozilla.org/pub/firefox/releases/52.9.0esr/win32/en-US/) (en-US)
- [Download link](https://ftp.mozilla.org/pub/firefox/releases/52.9.0esr/win32/) (More languages)

## Notes

You may want to disable updates and/or not installing the update tool since
any update will render Firefox inoperable on Windows XP.

## Screenshots

![Firefox main page](./pics/firefox/firefox-00.jpg)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: internet browser
