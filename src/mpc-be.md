# Media Player Classic (MPC-BE)

Media Player Classic is a fully-featured multimedia player with a look and feel
similar to that of Windows 98/ME mplayer.

## Info & Download

- [MPC-BE homepage](https://sourceforge.net/projects/mpcbe/)
- Version: 1.4.6
- [Download link](https://sourceforge.net/projects/mpcbe/files/MPC-BE/Release%20builds/1.4.6/MPC-BE.1.4.6.x86-installer.zip/download)

## Description

(From MPC-BE homepage)   
Media Player Classic - BE is based on the original "Media Player Classic"
project (Gabest) and "Media Player Classic Home Cinema" project (Casimir666),
contains additional features and bug fixes.

## Screenshots

![MPC-BE dark theme](./pics/mpc-be/mpc-be-00.png)

MPC-BE default "black" theme.

![MPC-BE light theme](./pics/mpc-be/mpc-be-01.png)

MPC-BE classic "light" theme.

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: multimedia
