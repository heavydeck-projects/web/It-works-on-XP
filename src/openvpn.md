# OpenVPN

OpenVPN is a multiplatform, free and open source software for creating Virtual
Private Networks (VPN).

## Info & Download

- [OpenVPN homepage](https://openvpn.net/)
- Version: 2.3.17
- [Download link](https://build.openvpn.net/downloads/releases/openvpn-install-2.3.17-I001-i686.exe)

## Description

(From [wikipedia](https://en.wikipedia.org/wiki/OpenVPN))   
OpenVPN is a virtual private network (VPN) system that implements techniques to
create secure point-to-point or site-to-site connections in routed or bridged
configurations and remote access facilities. It implements both client and
server applications.

OpenVPN allows peers to authenticate each other using pre-shared secret keys,
certificates or username/password. When used in a multiclient-server
configuration, it allows the server to release an authentication certificate
for every client, using signatures and certificate authority.

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: vpn
