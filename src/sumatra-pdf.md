# Sumatra PDF

Sumatra PDF is a lightweight PDF (and other format) document reader.

## Info & Download

- [Sumatra PDF homepage](https://www.sumatrapdfreader.org)
- By: Krzysztof Kowalczyk
- Version: 3.1.2
- [Download link](https://www.sumatrapdfreader.org/download-prev.html)

## Description

(From Sumatra PDF homepage)

What is Sumatra PDF?    
Sumatra PDF is a free PDF, eBook (ePub, Mobi), XPS, DjVu, CHM, Comic Book (CBZ
and CBR) reader for Windows.

Sumatra PDF is powerful, small, portable and starts up very fast.

Simplicity of the user interface has a high priority.

## Screenshots

![Interface](./pics/sumatra-pdf/sumatra-pdf-00.png)

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: pdf ebook
