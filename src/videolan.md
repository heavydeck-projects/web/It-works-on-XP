# Videolan (VLC)

VLC is a fully-featured multimedia player.

## Info & Download

- [VLC homepage](https://www.videolan.org/)
- Version: 3.0.11 (Latest version as of post date)
- [Download link](https://www.videolan.org/)

## Description

(From VLC homepage)   

VLC is a free and open source cross-platform multimedia player and
framework that plays most multimedia files, and various streaming
protocols.

- Plays everything - Files, Discs, Webcams, Devices and Streams.
- Plays most codecs with no codec packs needed - MPEG-2, MPEG-4, H.264, MKV, WebM, WMV, MP3...
- Runs on all platforms - Windows, Linux, Mac OS X, Unix, iOS, Android ...
- Completely Free - no spyware, no ads and no user tracking.

## Screenshots

![VLC classic interface](./pics/videolan/videolan-00.png)

VLC classic interface.

![VLC skinned interface](./pics/videolan/videolan-01.png)

VLC skinned interface.

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: multimedia
