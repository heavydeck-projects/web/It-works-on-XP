# Java 8 JRE & JDK

Java is a class-based, object-oriented programming language.

## Info & Download

- [Java homepage](https://java.com/)
- By: [Oracle (formerly SUN Microsystems)](https://www.oracle.com/)
- Version: 8u152
- [JRE download link](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) **(requires login; Free registration)**
- [JDK download link](https://www.oracle.com/java/technologies/javase/javase8-archive-downloads.html) **(requires login; Free registration)**

## Description

(From [wikipedia](https://en.wikipedia.org/wiki/Java_programming_language))    
Java is a class-based, object-oriented programming language that is designed to
have as few implementation dependencies as possible. It is a general-purpose
programming language intended to let application developers write once, run
anywhere (WORA), meaning that compiled Java code can run on all platforms that
support Java without the need for recompilation. Java applications are
typically compiled to bytecode that can run on any Java virtual machine (JVM).

## Notes

During setup you may ignore the "Unsupported OS" warning.

## Contribution

- J.Luis ([site](http://heavydeck.net/))

;tags: java development-kit runtime
