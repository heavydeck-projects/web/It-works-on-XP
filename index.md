## About this site

Finding the exact software revision that still works on that legacy system that
it is *still* running Windows XP is becoming increasingly hard, so this site
tries to provide links to that particular software and software version that
will still work on the old Microsoft OS.

### It's easy to contribute!

- Submit new entries using git via [GitHub](https://github.com/).
- You may also improve existing entries or add an image to those without them.
- Donate to the individual people who contribute pages whose names are at the bottom of each page.
