# Software name

A really short description of the software itself.

## Info & Download

- [Software name homepage](https://hasefroch.com/software-name/)
- By: [The company or person that made the software](https://hasefroch.com/)
- Version: 1.3.3.7 (Latest version as of 2021/04/08)
- [Download link](https://hasefroch.com/installer.exe)
- [Mirror Link (only if original download is not available)](https://some-other-site-listing/no-direct-links-to-exe)

## Description

A very long description of the software, go into details if you want to!
(optional but very recommended)

## Notes

Some warnings or steps that may be followed during installation (optional)

## Screenshots

![sample shot 00](./pics/software-name/software-name-00.webp)
A description (optional)

![sample shot 01](./pics/software-name/software-name-01.png)
A description (optional)

![sample shot 02](./pics/software-name/software-name-02.pg)
A description (optional)

## Contribution

- Author Name ([site](http://author-site.loc/))
- Contributor Name ([github](http://github.com/you-know-the-drill/))
- Contributor Name ([site](http://another-site.loc/))

;tags: tags separated by spaces tag-with-a-space
